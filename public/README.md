# Distributed Memories



## [AURAe](https://gaite-lyrique.net/storage/2022/03/28/gl-feuille-de-salle-aurae-web.pdf) 
### [@La Gaiété Lyrique] (https://gaite-lyrique.net/en/event/aurae-sabrina-ratte) 


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3003.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3010.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3014.jpg)



# Interactivité



![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3017.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3018.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3043.jpg)



# Mémoire


![](medias/distributed_memories_thumb_5000.jpg)



# Mouvement


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3048.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3033.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3066.jpg)


![](medias/Aurae_-_Voyez-Vous_(Vinciane_Lebrun)_-3076.jpg)


![](medias/277591538_1902401949952322_3634419360351585550_n.jpg)



# Espace 


<video data-autoplay muted loop  src="./medias/mp4/1687_loop.mp4"></video>


<video data-autoplay muted loop src="./medias/mp4/1696_loop.mp4"></video>



##### Distributed Memories 
     
#### [Sabrina Ratté](http://sabrinaratte.com)  
#### [Guillaume Arseneault](http://gllm.ca)

avec

[Roger Tellier Craig](https://rogertelliercraig.com/) aux sons

Photos par [Vinciane Lebrun](http://www.voyez-vous.tv/)

